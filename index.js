const input = document.querySelector("#photos");
const divPhotos = document.querySelector(".photos");

let Files = [];

const dataTransferInit = () => {
    const dataTransfer = new DataTransfer();
    Files.forEach((file) => dataTransfer.items.add(file));
    input.files = dataTransfer.files;
};

const deletePhoto = (e) => {
    const idx = Files.indexOf(Files.find((el) => el.name === e.target.alt));
    Files.splice(idx, 1);
    e.target.remove();
    dataTransferInit();
};

input.addEventListener("change", (evt) => {
    const [file] = input.files;
    if (file) {
        const img = document.createElement("img");
        img.src = URL.createObjectURL(file);
        img.alt = file.name;
        img.addEventListener("click", deletePhoto);
        divPhotos.append(img);
        Files.push(file);
        dataTransferInit();
    }
});
